# 基于Spring Boot + Spring Cloud实现的电商网站项目源代码与数据库

## 项目描述

本项目是一个基于Spring Boot和Spring Cloud实现的电商网站项目源代码与数据库。项目旨在综合应用Spring Cloud进行微服务架构开发，涵盖了用户管理、商品管理等多个模块。通过本项目，您可以深入了解Spring Cloud在实际项目中的应用，并掌握微服务架构的开发流程。

## 项目特点

- **微服务架构**：采用Spring Cloud进行微服务架构开发，模块化设计，便于扩展和维护。
- **实战项目**：结合电商网站的实际需求，涵盖用户管理、商品管理等多个模块。
- **开发环境**：适用于Windows操作系统，Java环境为JDK1.8，数据库为MySQL 5.5以上。

## 开发环境

- **操作系统**：Windows
- **Java环境**：JDK1.8（请勿使用高版本）
- **开发工具**：IntelliJ IDEA 2020
- **数据库**：MySQL 5.5以上
- **Spring Cloud版本**：Greenwich.SR2
- **Spring Boot版本**：2.1.7 Release

## 项目结构

- **common模块**：公共模块，包含一些通用的工具类和配置。
- **eureka-server**：服务注册中心，用于服务的注册与发现。
- **user-provider**：用户服务提供者，负责用户相关的业务逻辑。
- **user-consumer**：用户服务消费者，负责调用用户服务提供者的接口。
- **goods-provider**：商品服务提供者，负责商品相关的业务逻辑。

## 使用说明

1. **数据库配置**：
   - 在本地MySQL数据库中创建数据库，并导入`b2bdata.sql`和`b2bgoods.sql`文件。
   - 修改`application.yml`文件中的数据库连接配置，确保数据库连接密码正确。

2. **启动服务**：
   - 首先启动`eureka-server`服务。
   - 然后启动`user-provider`服务，注意修改数据库连接密码。
   - 最后启动`user-consumer`服务。

3. **访问测试**：
   - 启动完成后，访问`http://localhost:8893/admin/tologin`，使用用户名`王三`和密码`123`进行登录。

4. **商品服务配置**：
   - 修改`goods-provider`模块中的`application.yml`文件，确保数据库配置正确。
   - 运行`goods-provider`模块中的单元测试程序，验证商品服务的功能。

## 注意事项

- 请确保使用JDK1.8版本，高版本JDK可能导致兼容性问题。
- 数据库配置时，请确保MySQL版本为5.5以上，并正确配置数据库连接信息。
- 在启动各个服务时，请确保服务注册中心`eureka-server`已正常启动。

## 贡献与反馈

如果您在使用过程中遇到任何问题或有任何建议，欢迎提交Issue或Pull Request。我们非常欢迎您的贡献！

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。